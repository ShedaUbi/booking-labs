exports.up = async knex => {
    await knex.raw(
        `CREATE TABLE \`groups\` (
            \`id\` INT(10) NOT NULL AUTO_INCREMENT,
            \`group_name\` VARCHAR(64) NOT NULL,
            PRIMARY KEY (\`id\`) USING BTREE
        )`,
    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};