exports.up = async knex => {
    await knex.raw(
        `CREATE TABLE \`users\` (
            \`id\` INT(10) NOT NULL AUTO_INCREMENT,
            \`login\` VARCHAR(64) NOT NULL,
            \`password\` VARCHAR(64) NOT NULL,
            \`surname\` VARCHAR(64) NOT NULL,
            \`teacher\` TINYINT(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (\`id\`) USING BTREE
        )`,
    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};