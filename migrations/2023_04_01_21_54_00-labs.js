exports.up = async knex => {
    await knex.raw(
        `CREATE TABLE \`labs\` (
            \`id\` INT(10) NOT NULL AUTO_INCREMENT,
            \`id_user\` INT(10) NOT NULL,
            \`id_group\` INT(10) NOT NULL,
            \`course_name\` VARCHAR(256) NOT NULL,
            \`lesson_name\` VARCHAR(256) NOT NULL,
            \`variants\` INT(10) NOT NULL,
            \`variants_users\` INT(10) NOT NULL DEFAULT '1',
            \`start_at\` DATETIME NOT NULL,
            \`end_at\` DATETIME NOT NULL,
            PRIMARY KEY (\`id\`) USING BTREE
        )`,
    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};