exports.up = async knex => {
    await knex.raw(
        `CREATE TABLE \`connections\` (
            \`id_user\` INT(10) NOT NULL,
            \`id_group\` INT(10) NOT NULL            
        )`,
    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};