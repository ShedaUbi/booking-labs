exports.up = async knex => {
    await knex.raw(

        `CREATE TABLE \`invitations\` (
            \`id\` INT(10) PRIMARY KEY AUTO_INCREMENT,
            \`sender_id\` INT(10) NOT NULL,
            \`receiver_id\` INT(10) NOT NULL,
            \`code\` VARCHAR(256) NOT NULL,
            \`created_at\` DATETIME NOT NULL
          )`,

    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};