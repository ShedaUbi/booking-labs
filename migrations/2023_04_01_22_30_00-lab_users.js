exports.up = async knex => {
    await knex.raw(
        `CREATE TABLE \`lab_users\` (
            \`id_user\` INT(10) NOT NULL,
            \`id_lab\` INT(10) NOT NULL,
            \`variant\` INT(10) NOT NULL,
            \`took_at\` DATETIME NOT NULL,
            PRIMARY KEY (\`id_user\`, \`id_lab\`) USING BTREE
        )`,
    );
};

exports.down = async function down() {
    throw new Error('No steps back');
};