const path = require('path');
const express = require('express');
const knex = require('./knex');
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

class App {
    constructor() {}

    async start() {
        try {
            let [, migrated] = await knex.migrate.latest();
            for (let i = 0; i < migrated.length; i++) {
                console.debug('\tMigrate apply: ' + migrated[i]);
            }
        console.debug('\tDataBase migration: OK');
        } catch (e) {
            console.error('\tDataBase migration: failed.\n' + e.message);
            return;
        }
        
        passport.serializeUser(function(user, done) {
            console.log("Serialize: ", user.id);
            done(null, user.id);
        });
        //Получаем id пользователя
        passport.deserializeUser(async function(id, done) {
            const user = await knex('users')
                .where({ id })
                .first();                             
                done(null, user ?? null);
        });
        //Заменяем стандартный атрибут username usernameField: 'email'
        //Получаем данные переданные методом POST из формы email/password
        //Параметр done работает на подобии return он возвращает пользователя или false в зависимости прошел ли пользователь аутентификацию
        passport.use(new LocalStrategy({	usernameField: 'email'	}, async function(email, password, done) {
            const user = await knex('users')
            .where({ login: email, password })
            .first();
            console.log(email, password);
            //Строим запрос в базу данных, ищем пользователя по email переданному из формы в стратегию
            if (!user) {
                console.log('User not found');
                return done(null, false);                
            }
            //Иначе передаем выводим найденый email в консоль и передаем пользователя функцией done в сессию
            else {
                console.log(user.login);
                return done(null, user);
                
            }
            
        }));
        
        this.app = express();
        this.app.set('views', './public/views');
        //this.app.set('views', __dirname + '/public/views');
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
        this.app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
        //this.app.set('views', __dirname + '/public');
        //this.app.engine('html', require('ejs').renderFile);
        this.app.set('view engine', 'ejs');
        this.app.use(express.json()); //Переводим все полученные данные в объекты json
        this.app.use(express.urlencoded({extended: false}));//Запрещаем формировать массивы(Если передаете массив данных,лучше поставить true)
        this.app.use(session({
            secret: "secret", //Задаем ключ сессий
            store: new FileStore(), //Указываем место хранения сессий(Используя этот пакет,у вас будет создана папка sessions, в которой будут хранится сессии и, даже если сервер перезагрузится,пользователь останется авторизованным
            cookie: {
                path: "/",
                httpOnly: true, // path - куда сохранять куки, httpOnly: true - передача данных только по https/http,maxAge - время жизни куки в миллисекундах 60 * 60 * 1000 = 1 час 
                maxAge: 60 * 60 * 1000
            },

            resave: false,
            saveUninitialized: false
        }));
        
        this.app.use(passport.initialize()); //Инициализируем паспорт
        this.app.use(passport.session()); //Синхронизируем сессию с паспортом
        //Проверяем если авторизован - пропускаем дальше,если нет запрещаем посещение роута
        const logout = (req,res,next) => {
            if(req.isAuthenticated()) {
                return res.redirect('/admin');
            } else {
                next()
            }
        }

        this.app.get('/', (req, res) => {
            res.render('index');
        });        
        this.app.get('/login', (req, res) => {
            //сделать проверку на залогиненность?
            res.render('log');
        });

        const auth = (req,res,next) => {
            if(req.isAuthenticated()) {
                next()
            } else {
                return res.redirect('/');
            }
        }

        this.app.post('/login', function(req, res, next) {
            passport.authenticate('local', function(err, user) {
              if (err) {
                  return next(err);
              }
              if (!user) {
                console.log(user);
                return res.redirect('/');
              }
              req.logIn(user, function(err) {
                if (err) { 
                    return next(err);
                }
                return res.redirect('/personal_account');
              });
            })(req, res, next);
        });

        this.app.get('/personal_account', auth, async (req, res) => {
        const userId = req.session.passport.user;
        
        // Получение массива лабораторных работ, на которые уже записан пользователь.
        const labUsers = await knex('lab_users').where({ id_user: userId });
        const labIds = labUsers.map(labUser => labUser.id_lab);
        
        // Получение массива лабораторных работ, на которые пользователь еще не записан.
        const inconnections = await knex('connections').where({ id_user: userId }).select('id_group');
        const inlabs = await knex('labs')
            .where({ id_group: inconnections[0].id_group })
            .whereNotIn('id', labIds)
            .select(['course_name', 'lesson_name', 'variants', 'variants_users', 'start_at', 'end_at', 'id'])
            .orderBy('start_at', 'asc');
        
        const name = await knex('users').where({ id: userId }).select('surname');
        res.render('pa', { inlabs: inlabs, name: name[0].surname });
        });
    
        this.app.get('/booking/:id', auth, async (req, res) => {
        const id = req.params.id;
        const message = req.query.message;
        const userId = req.session.passport.user;
        const idlab = await knex('labs')
            .where({ id: id })
            .select(['id', 'variants', 'variants_users'])
            .first();
            //const userId = req.session.passport.user;
        res.render('bg', { bookingId: idlab, message: message});
        });

        this.app.post('/booking/:id', auth, (req, res) => {
            const selectedButtonId = req.body.id;
            console.log('Id лабы: ' + selectedButtonId);
            res.redirect(`/booking/${selectedButtonId}`); //Мы меняем путь только при редиректе после обработки POST-запроса, чтобы перейти на страницу с бронированием. Вот почему внутри POST-запроса путь всё ещё "/booking/:id".
            });

          /*this.app.post('/invite/:id', (req, res) => {
            const selectedButtonId = req.params.id;
            console.log('инвайт ' + selectedButtonId);            
          });*/

        this.app.get('/completed/', auth, async (req, res) => {
        const userId = req.session.passport.user;
        //const selectedButtonId = req.query.selectedButtonId;
        //const selectedButtonId = req.query.numlab;
    
        const labs = await knex('lab_users')
        .select('lab_users.id_lab', 'lab_users.variant', 'l.course_name', 'l.lesson_name', 'l.end_at')
        .leftJoin('labs as l', 'l.id', 'lab_users.id_lab')
        .where('lab_users.id_user', userId);

        const users = await knex.select('lu.id_lab', 'u.surname')
        .from('lab_users as lu')
        .leftJoin('users as u', 'u.id', 'lu.id_user')
        .whereNot('lu.id_user', userId)
        .whereIn(['lu.id_lab', 'lu.variant'], labs.map(lab => [lab.id_lab, lab.variant]));
        
        const name = await knex('users')
        .where({ id: userId })
        .select('surname');
        
        const results = labs.map(lab => ({
            id: lab.id_lab,
            variant: lab.variant,
            course_name: lab.course_name,
            lesson_name: lab.lesson_name,
            end_at: lab.end_at,
            users: users.filter(user => user.id_lab === lab.id_lab).map(user => user.surname),
        }));

        console.log(results, name);
    
        res.render('completed', {results, name: name[0].surname});
    });

    this.app.post('/completed/:id', auth, async (req, res) => {
        const selectedButtonId = req.body.variant;
        const userId = req.session.passport.user;
        const numlab = req.params.id;
        const date = (new Date()).toISOString().slice(0, 19).replace('T', ' ');
        

        const lab = await knex('labs')
            .select('end_at')
            .where('id', numlab)
            .first();
    
        if (Date.parse(date) > Date.parse(lab.end_at)) {
            console.log('Ошибка: текущая дата превышает end_at для данной лабы');
            return;
            //Есть одна проблема, если забыть открытую страницу на выборе варианта, но кукам похер (куки тебя не вышвырнут еще), но после истечения end_at, вариант перезапишется :((((((
        }
        
        const maxVariantsUsers = (await knex('labs')
            .where({id: numlab})
            .select('variants_users')
            .first()).variants_users;

        // Проверяем, что выбранный вариант не занят другим пользователем    
        const duplicateCount = await knex('lab_users')
            .where({ id_lab: numlab, variant: selectedButtonId });

        if (duplicateCount.length >= maxVariantsUsers) {
            const errorMessage = encodeURIComponent('Извините, этот вариант уже занят');
            res.redirect(`/booking/${numlab}?message=${errorMessage}`);
            return;
        }

        const count = await knex('lab_users')
            .where('id_user', userId)
            .where('id_lab', numlab)
            .count('* as cnt')
            .first();
    
        if (count.cnt > 0) {
            // запись уже существует, обновляем
            await knex('lab_users')
                .where('id_user', userId)
                .where('id_lab', numlab)
                .update({                       
                    variant: selectedButtonId,
                    took_at: date
                });
        } else {
            // запись не существует, вставляем новую
            await knex('lab_users').insert({
                id_user: userId,
                id_lab: numlab,
                variant: selectedButtonId,
                took_at: date
            });
        }
        
        console.log('Id лабы: ' + numlab, 'Номер варианта: ' + selectedButtonId, 'Id пользователя: ' + userId, 'Дата: ' + date);
        res.redirect(`/completed/`);
    });

        //Проверяем если пользователь авторизован - пропускаем,если нет - возвращаем к форме
        //Если роут /logout выкидываем пользователя из сессии и перекидываем на форму
        this.app.get('/logout', (req,res) => {
            req.logout();
            res.redirect('/');
        });
          
        /*
        this.app.post('/login', urlencodedParser, async (req, res) => {
            const email=req.body.email;
            const password=req.body.password;
            
            const row = await knex('users')
                .where({ login: email, password })
                .first();

            if (row) {
                res.redirect("http://localhost:3333/personal_account");    
            } else {
                res.send("<h2>Неверный логин или пароль!</h2>");                
            }

        });
        */

        this.app.listen(3333, () => {
            console.log('Application listening on port 3333!');
        });
    }
}

const app = new App();
app.start().catch(console.error);